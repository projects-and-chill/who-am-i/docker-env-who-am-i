
### BASE COMMAND:
```cmd
make up <env>      #up docker-compose environment
make down <env>    #down docker-compose environment
make clear <env>   #clear, container, images, volumes and network of environment
```

### DEPLOY ENVIRONMENTS:

- DEV
```bash
make clone
make init
make up dev
```

- STAGING
```bash
make clone
make up staging
```

- PROD
```bash
make clone
make up prod
```

### DOCKER BUILD & PUSH:

```bash
export IMAGE="<DOCKER-REGISTRY>/<DOCKER-IMAGE-NAME>" && \
docker build -t $IMAGE:latest -t $IMAGE:1.0 services/<service-name> && \
docker push --all-tags $IMAGE
```
