ifeq (up,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (down,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (clear,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

PROJECT_DIR = "./"
ENV_FILE = "./environments/$(RUN_ARGS)/.env.$(RUN_ARGS)"
COMPOSE_FILE_1 = "./docker-compose.yml"
COMPOSE_FILE_2 = "./environments/$(RUN_ARGS)/docker-compose.$(RUN_ARGS).yml"

#-------------------Init dev--------------------------

clone:
	git clone git@gitlab.com:projects-and-chill/who-am-i/api-who-am-i.git ./services/api-service
	git clone git@gitlab.com:projects-and-chill/who-am-i/front-who-am-i.git ./services/front-service
	git clone git@gitlab.com:projects-and-chill/who-am-i/database-who-am-i.git ./services/database-service

init:
	make --directory="./services/api-service" init
	make --directory="./services/front-service" init


#-------------------Up environment--------------------------
.PHONY: up
up:
	  docker-compose --project-directory $(PROJECT_DIR) --env-file $(ENV_FILE) -f $(COMPOSE_FILE_1)  -f $(COMPOSE_FILE_2) up



#-------------------Down environment--------------------------
.PHONY: down
down:
	  docker-compose --project-directory $(PROJECT_DIR) --env-file $(ENV_FILE) -f $(COMPOSE_FILE_1) -f $(COMPOSE_FILE_2) down -v


#-------------------Clear environment--------------------------
.PHONY: clear
clear:
	  docker-compose --project-directory $(PROJECT_DIR) --env-file $(ENV_FILE) -f $(COMPOSE_FILE_1) -f $(COMPOSE_FILE_2) down -v --rmi "all"

